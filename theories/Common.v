From Cats Require Import Observational Sprop.

Set Primitive Projections.
Set Universe Polymorphism.
Set Observational Inductives.


Polymorphic Record sigma {A : Type} {B : A -> Type} :=
  pair { π1 : A ; π2 : B π1 }.
Arguments sigma : clear implicits.
Notation "'∑' x .. y ',' B" := (sigma _ (fun x => .. (sigma _ (fun y => B)) .. )) (at level 50, x binder, y binder, B at level 100).

Lemma pair_cong {A B} (p q : ∑ x : A, B x) (e1 : π1 p ~ π1 q) : π2 p ~[ap B e1] π2 q -> p ~ q.
Proof.
  destruct p,q; cbn in *.
  induction e1; cbn; intros e; induction e; reflexivity.
Qed.