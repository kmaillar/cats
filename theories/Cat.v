From Cats Require Import Observational Sprop Common.

Set Observational Inductives.
Set Primitive Projections.
Set Universe Polymorphism.

Record catStr {A : Type} :={
  hom : A -> A -> Type ;
  comp : forall x y z, hom x y -> hom y z -> hom x z  ;
  id : forall x, hom x x ;
}.

Arguments catStr : clear implicits.
Arguments comp {_ _ _ _ _} _ _.
Arguments id {_} _ _, {_ _ _}.
Notation "f ∘ g" := (comp g f) (at level 45, left associativity).
Notation " C '⟨' x ',' y '⟩' " := (@hom _ C x y) (at level 90).

Record catProp {A} {C : catStr A} : SProp := {
  assoc : forall {x y z w} (f : C⟨x, y⟩) (g : C⟨y, z⟩) (h : C⟨z, w⟩),
    h ∘ (g ∘ f) ~ h ∘ g ∘ f ;
  lunit : forall {x y} (f : C⟨x,y⟩), id ∘ f ~ f ;
  runit : forall {x y} (f : C⟨x,y⟩), f ∘ id ~ f
} .
Arguments catProp : clear implicits.
Arguments catProp {_} _.

Record cat := { 
  carrier :> Type ; 
  categoryStr :> catStr carrier ;
  categoryEqs :> catProp categoryStr ;
}.

Record functorStr {A B : cat} := {
  onObject :> A -> B ;
  map : forall {x y}, A⟨x,y⟩ -> B⟨onObject x, onObject y⟩
}.
Arguments functorStr : clear implicits.

Record functorProp {A B : cat} {F: functorStr A B} : SProp := {
  mapComp : forall {x y z} (f : A⟨x,y⟩) (g : A⟨y,z⟩),
    map F (g ∘ f) ~ map F g ∘ map F f ;
  mapId : forall x, map F (id A x) ~ id
}.
Arguments functorProp : clear implicits.
Arguments functorProp {_ _} _.

Record functor {A B : cat} := {
  functorStructure :> functorStr A B ;
  functorEqs :> @functorProp A B functorStructure
}.
Arguments functor : clear implicits.

Lemma functorStr_cong0 {A B : cat} (F G : functorStr A B)
  (hobj : onObject F ~ onObject G)
  (hmap : forall x y (f : A⟨x,y⟩), 
    map F f ~[ap2 (hom B) (ap (fun h => h x) hobj) (ap (fun h => h y) hobj)] map G f) :
  F ~ G.
Proof.
  destruct F as [F mapF], G as [G mapG]; cbn in *.
  induction hobj; cbn in *.
  apply ap; repeat (apply funext; intros); apply hmap.
Qed.

Lemma functorStr_cong {A B : cat} (F G : functorStr A B)
  (hobj : forall a : A, F a ~ G a)
  (hmap : forall x y (f : A⟨x,y⟩), 
    map F f ~[ap2 (hom B) (hobj x) (hobj y)] map G f) :
  F ~ G.
Proof.
  assert (H : onObject F ~ onObject G) by apply funext, hobj.
  now apply (functorStr_cong0 F G H).
Qed.

Lemma functor_cong {A B : cat} (F G : functor A B)
  (hobj : forall a : A, F a ~ G a)
  (hmap : forall x y (f : A⟨x,y⟩), 
    map F f ~[ap2 (hom B) (hobj x) (hobj y)] map G f) :
  F ~ G.
Proof.
  destruct F as [F hF], G as [G hG]; cbn in *.
  induction (functorStr_cong F G hobj hmap); reflexivity.
Qed.


Definition functorCompStr {A B C : cat} (F : functorStr A B) (G : functorStr B C) : functorStr A C :=
  {| onObject x := G (F x) ; map x y f := map G (map F f) |}.

Lemma functorCompProp {A B C : cat} (F : functor A B) (G : functor B C) :
  functorProp (functorCompStr F G).
Proof.
  constructor; cbn; intros.
  (* Too annoying to proceed without rewrite; 
    TODO: discuss with Nico about that *)
  - eapply obseq_trans; [eapply ap, mapComp, F| eapply mapComp, G].
  - eapply obseq_trans; [eapply ap, mapId, F| eapply mapId, G].
Qed.

Definition functorIdStr (A : cat) : functorStr A A :=
  {| onObject x := x ; map _ _ f := f |}.

Lemma functorIdProp (A : cat) : functorProp (functorIdStr A).
Proof. constructor; cbn; reflexivity. Qed.

Definition CatStr : catStr cat :=
  {| hom A B := functor A B; 
    comp _ _ _ F G := {| 
      functorStructure := functorCompStr F G ;
      functorEqs := functorCompProp F G
    |} ;
    id A := {|
      functorStructure := functorIdStr A ;
      functorEqs := functorIdProp A
    |}
 |}.

Lemma CatProp : catProp CatStr.
Proof.  constructor; reflexivity. Qed.

Definition Cat : cat := {| carrier := cat ; categoryStr := CatStr ; categoryEqs := CatProp |}.

Definition invertible {A} {C : catStr A} {x y} (f : C⟨x,y⟩) :=
  ΣT g : C⟨y,x⟩, f ∘ g ~ id ∧ g ∘ f ~ id.

Definition invertible_uniq (C : cat) {x y} (f : C⟨x,y⟩) (g1 g2 : invertible f) :
  g1 ~ g2.
Proof.
  destruct g1 as (g1 & ? & rg1), g2 as (g2 &sg2 & ?).
  eapply wit_cong, obseq_trans; cbn. 1: apply obseq_sym, runit, C.
  eapply obseq_trans. 1: eapply (ap (fun h => g1 ∘ h)), obseq_sym, sg2.
  eapply obseq_trans. 1: eapply assoc, C.
  eapply obseq_trans. 2: eapply lunit, C.
  eapply ap, rg1.
Qed.

Lemma id_invertible (C : cat) (x : C) : invertible (id C x).
Proof. exists id; split; apply lunit, C. Defined.

Notation "g ∘-" := (fun h => g ∘ h) (at level 45).
Notation "-∘ f" := (fun h => h ∘ f) (at level 45). 

Lemma comp_invertible (C : cat) {x y z} (f : C⟨x,y⟩) (g : C⟨y,z⟩) :
  invertible f -> invertible g -> invertible (g ∘ f).
Proof.
  intros (f' & sf & rf) (g' & sg & rg); exists (f' ∘ g'); split.
  (* REALLY NEEDS REWRITE *)
  - eapply obseq_trans.  2: eapply sg.
    eapply obseq_trans. 1: eapply assoc, C.
    eapply (ap (-∘ g')), obseq_trans. 2: eapply runit, C.
    eapply obseq_trans. 1: eapply obseq_sym, assoc, C.
    eapply (ap (g ∘-) sf).
  - eapply obseq_trans.  2: eapply rf.
    eapply obseq_trans. 1: eapply assoc, C.
    eapply (ap (-∘ f)), obseq_trans. 2: eapply runit, C.
    eapply obseq_trans. 1: eapply obseq_sym, assoc, C.
    eapply (ap (f' ∘-) rg).
Defined.

Definition iso {A} (C : catStr A) : A -> A -> Type :=
  fun x y => ∑ (f : C⟨x,y⟩), invertible f.

Lemma iso_inj (C : cat) {x y} (f g : iso C x y) :
  f.(π1) ~ g.(π1) -> f ~ g.
Proof.
  destruct f as [f invf], g as [g invg]; cbn.
  intros eq; unshelve eapply pair_cong; cbn.
  1: assumption.
  induction eq; cbn; apply invertible_uniq.
Qed.

Definition coreStr (C : cat) : catStr C :=
  {| 
    hom := iso C ; 
    comp _ _ _ f g := {| π2 := comp_invertible C _ _ f.(π2) g.(π2) |} ;
    id x := {| π2 := id_invertible C x |}
  |}.

Lemma coreProp (C : cat) : catProp (coreStr C).
Proof.
  constructor; intros; apply iso_inj; cbn.
  - apply assoc, C.
  - apply lunit, C.
  - apply runit, C.
Qed.

Definition core (C : cat) : cat := 
  {| carrier := C ; categoryStr := coreStr C ; categoryEqs := coreProp C |}.


Definition opStr {A} (C : catStr A) : catStr A :=
  {| hom x y := C⟨y,x⟩ ; comp _ _ _ g f := g ∘ f ; id x := id |}.

Definition opProp (C : cat) : catProp (opStr C).
Proof.
  constructor; intros; cbn.
  - apply obseq_sym, assoc, C.
  - apply runit, C.
  - apply lunit, C.
Qed.

Definition op (C : cat) : cat :=
  {| carrier := C ; categoryStr := opStr C ; categoryEqs := opProp C |}.


(*  Injections from core to the base category and op *)

Definition iFunctorStr (C : cat) : functorStr (core C) C :=
  {| onObject (x : core C) := x : C ; map x y f := f.(π1) |}.

Lemma iFunctorProp (C : cat) : functorProp (iFunctorStr C).
Proof.  constructor; reflexivity. Qed.

Definition iFunctor C : functor (core C) C :=
  {| functorStructure := iFunctorStr C ; functorEqs := iFunctorProp C |}.


Definition iopFunctorStr (C : cat) : functorStr (core C) (op C) :=
  {| onObject (x : core C) := x : op C ; map x y f := f.(π2).(wit) |}.

Lemma iopFunctorProp (C : cat) : functorProp (iopFunctorStr C).
Proof. constructor; cbn; reflexivity. Qed.

Definition iopFunctor C : functor (core C) (op C) :=
  {| functorStructure := iopFunctorStr C ; functorEqs := iopFunctorProp C |}.

(* op endofunctor on Cat *)

Definition opMapStr {A B : cat} (F : functor A B) : functorStr (op A) (op B) :=
  {| onObject (x : op A) := (F x : op B) ; map _ _ f := map F f |}.

Lemma opMapProp {A B : cat} (F : functor A B) : functorProp (opMapStr F).
Proof. constructor; cbn; intros.
  - apply mapComp, F.
  - apply mapId, F.
Qed.

Definition opMap {A B : cat} (F : functor A B) : functor (op A) (op B) :=
  {| functorStructure := opMapStr F ; functorEqs := opMapProp F |}.

Definition opFunctorStr : functorStr Cat Cat :=
  {| onObject (C : Cat) := (op C : Cat) ; map _ _ F := opMap F |}.

Lemma opFunctorProp : functorProp opFunctorStr.
Proof. constructor; intros; unshelve eapply functor_cong; cbn; reflexivity. Qed.

Definition opFunctor : functor Cat Cat :=
  {| functorStructure := opFunctorStr; functorEqs := opFunctorProp |}.



(* Contravariant functor *)

Definition contrafunctor (A B : cat) := functor A (op B).


(* Terminal cat *)

Definition termCatStr : catStr unit :=
  {| hom _ _ := unit ; comp _ _ _ _ _ := tt ; id _ := tt |}.

Lemma unit_uniq {x y : unit} : x ~ y.
Proof. destruct x, y; reflexivity. Qed.

Lemma termCatProp : catProp termCatStr.
Proof. constructor; intros; apply unit_uniq. Qed.

Definition termCat : cat :=
  {| carrier := unit ; categoryStr := termCatStr ; categoryEqs := termCatProp |}.

Definition toTermFunctorStr (C : cat) : functorStr C termCat :=
  {| onObject x := tt : termCat ; map _ _ _ := tt |}.

Definition toTermFunctorProp (C :cat) : functorProp (toTermFunctorStr C).
Proof. constructor; cbn; reflexivity. Qed.

Definition toTermFunctor (C : cat) : functor C termCat :=
  {| functorStructure := toTermFunctorStr C ; functorEqs := toTermFunctorProp C |}.

Lemma toTermUniq (C : cat) (F : functor C termCat) : F ~ toTermFunctor C.
Proof.  unshelve eapply functor_cong; intros ; apply unit_uniq. Qed.


(* Grothendieck construction *)

Module GrothendieckOpfibration.

Definition trDom {X Y : cat} {F G : Cat⟨X, Y⟩} {x y} : F ~ G ->
  Y⟨F x, y⟩ -> Y⟨G x, y⟩ := fun eq f => ap (fun H : functor X Y => Y⟨H x, y⟩) eq # f.

Lemma trDomPostComp {X Y : cat} {F G : functor X Y} {x y z} (h : Y⟨y,z⟩) 
  (eq : F ~ G) (f : Y⟨F x, y⟩) :
    h ∘ trDom eq f ~ trDom eq (h ∘ f).
Proof. induction eq; reflexivity. Qed.

Lemma trDomPreComp {X Y : cat} {F G : functor X Y} {x y z} (h : X⟨x,y⟩) 
  (eq : F ~ G) (f : Y⟨F y, z⟩) :
    trDom eq f ∘ map G h  ~ trDom eq (f ∘ map F h).
Proof. induction eq; reflexivity. Qed.

Lemma mapTrDom {X Y Z: cat} {F G : functor X Y} {x y}
  (eq : F ~ G) (f : Y⟨F x, y⟩) (H : Cat⟨Y, Z⟩) :
  map H (trDom eq f) ~ trDom (ap (H ∘-) eq) (map H f).
Proof. induction eq; reflexivity. Qed.


Record sectStr {C : cat} {B : functor C Cat} := 
  {
    sOnObject :> forall c, B c ;
    sMap : forall {x y} (f : C⟨x, y⟩), (B y)⟨map B f (sOnObject x), sOnObject y⟩
  }.
Arguments sectStr : clear implicits.
Arguments sectStr {_} _.

Record sectProp {C : cat} {B : functor C Cat} {sStr : sectStr B} : SProp :=
  { 
    sMapId : forall c,  trDom (mapId B c) (sMap sStr (id C c)) ~ id (B c) (sStr c) ;
    sMapComp : forall {x y z} (f : C⟨x,y⟩) (g : C⟨y,z⟩), 
      trDom (mapComp B f g) (sMap sStr (g ∘ f)) ~ sMap sStr g ∘ map (map B g) (sMap sStr f)
  }.
Arguments sectProp : clear implicits.
Arguments sectProp {_ _} _.

Definition sect {C} (B : functor C Cat) := ΣT (sStr : sectStr B), sectProp sStr.
Definition sect_str {C B} : @sect C B -> sectStr B := wit.
Coercion  sect_str : sect >-> sectStr.
Definition sect_eqs {C B} (s : @sect C B) : sectProp s := pf s.
Coercion  sect_eqs : sect >-> sectProp.

Section Grothendieck.
  Context (C : cat) (B : functor C Cat).

  Definition totalCarrier := ∑ c : C, B c.
  Definition totalHom (p q : totalCarrier) :=
    ∑ (f : C⟨π1 p, π1 q⟩), (B q.(π1))⟨map B f (π2 p), π2 q⟩.

  Definition totalComp (p q r : totalCarrier)
    (f : totalHom p q) (g : totalHom q r) : totalHom p r.
  Proof.
    exists (π1 g ∘ π1 f).
    eapply trDom. 1: eapply obseq_sym, mapComp, B. 
    exact (π2 g ∘ map (map B (π1 g)) (π2 f)).
  Defined.

  Definition totalId (p : totalCarrier) : totalHom p p.
  Proof.
    exists id.
    eapply trDom. 1: eapply obseq_sym, mapId, B.
    apply id.
  Defined.

  Definition totalCatStr : catStr totalCarrier :=
    {| hom := totalHom ; comp := totalComp; id := totalId |}.

  Lemma totalCatProp : catProp totalCatStr.
  Proof.
    constructor; intros; unshelve eapply pair_cong; cbn -[trDom].
    - eapply assoc, C.
    - eapply lunit, C.
    - eapply runit, C.
    - eapply obseq_trans. 1: apply cast_trans.
      eapply obseq_trans.
      2: eapply ap, obseq_sym, (trDomPreComp (F:= map B (π1 h) ∘ map B (π1 g)) (G:= map B (π1 h ∘ π1 g))).
      eapply htranspose, obseq_trans.
      1:{
        eapply obseq_trans. 
        1:eapply (ap (π2 h ∘-)), obseq_trans;
          [ apply mapTrDom | apply ap, (mapComp (map B (π1 h)))].
        eapply obseq_trans. 1: apply trDomPostComp.
        eapply ap, assoc, (B (π1 w)).
      }
      eapply htranspose, obseq_sym.
      eapply obseq_trans; [eapply cast_trans|].
      eapply obseq_trans; [eapply cast_trans|].
      eapply obseq_trans; [eapply cast_trans|].
      reflexivity.
    - eapply obseq_trans. 1: apply cast_trans.
      eapply htranspose, obseq_trans.
      apply (trDomPreComp (F:=id Cat _) (G:=map B id)).
      eapply htranspose, obseq_trans.
      1: apply lunit, (B (π1 y)).
      eapply obseq_sym, obseq_trans.
      1: apply cast_trans; cbn.
      reflexivity.
    - eapply obseq_trans. 1: apply cast_trans.
      cbn; set (e := obseq_trans _ _).
      eapply htranspose, obseq_trans.
      1:eapply (ap (_ ∘-)), mapTrDom.
      eapply obseq_trans.
      apply (trDomPostComp (F:=map B (π1 f)) (G:=map B (π1 f) ∘ map B id) (π2 f)).
      eapply htranspose.
      eapply obseq_trans.
      1: eapply (ap (π2 f ∘-)), mapId, (map B (π1 f)).
      eapply obseq_trans. 1: apply runit, (B (π1 y)).
      eapply obseq_sym, obseq_trans.
      1: eapply cast_trans.
      reflexivity.
  Qed.

  Definition totalCat : cat := 
    {| carrier := totalCarrier ; categoryStr := totalCatStr ; categoryEqs := totalCatProp |}.
  

  Definition projFunctorStr : functorStr totalCat C :=
    {| onObject (x : totalCat) := π1 x : C ; 
      map _ _ f := f.(π1)  
    |}.

  Definition projFunctorProp : functorProp projFunctorStr.
  Proof.  constructor; cbn; intros; reflexivity. Qed.

  Definition projFunctor : functor totalCat C :=
    {| functorStructure := projFunctorStr ; functorEqs := projFunctorProp |}.

End Grothendieck.
End GrothendieckOpfibration.

Module GrothendieckFibration.

Definition trCod {X Y : cat} {F G : (op Cat)⟨Y, X⟩} {x y} : F ~ G ->
  Y⟨x, F y⟩ -> Y⟨x, G y⟩ := fun eq f => ap (fun H : (op Cat)⟨Y, X⟩ => Y⟨x, H y⟩) eq # f.

Lemma trCodPostComp {X Y : cat} {F G : functor X Y} {x y z} (h : X⟨y,z⟩) 
  (eq : F ~ G) (f : Y⟨x, F y⟩) :
    map G h ∘ trCod eq f ~ trCod eq (map F h ∘ f).
Proof. induction eq; reflexivity. Qed.

Lemma trCodPreComp {X Y : cat} {F G : functor X Y} {x y z} (h : Y⟨x,y⟩) 
  (eq : F ~ G) (f : Y⟨y, F z⟩) :
    trCod eq f ∘ h  ~ trCod eq (f ∘ h).
Proof. induction eq; reflexivity. Qed.

Lemma mapTrCod {X Y Z: cat} {F G : functor X Y} {x y}
  (eq : F ~ G) (f : Y⟨x, F y⟩) (H : Cat⟨Y, Z⟩) :
  map H (trCod eq f) ~ trCod (ap (H ∘-) eq) (map H f).
Proof. induction eq; reflexivity. Qed.

Definition otrans {A a b c} := @obseq_trans A a b c.
Definition osym {A a b} := @obseq_sym A a b.

Tactic Notation "otrans" := eapply obseq_trans.
Tactic Notation "osym" := eapply obseq_sym.
Tactic Notation "oleft" tactic(t) := otrans ; [t|].
Tactic Notation "oright" tactic(t) := otrans ; [|t].

Section Grothendieck.
  Context (C : cat) (B : functor C (op Cat)).

  Definition totalCarrier := ∑ c : C, B c.
  Definition totalHom (p q : totalCarrier) :=
    ∑ (f : C⟨π1 p, π1 q⟩), (B p.(π1))⟨π2 p, map B f (π2 q)⟩.

  Definition totalComp (p q r : totalCarrier)
    (f : totalHom p q) (g : totalHom q r) : totalHom p r.
  Proof.
    exists (π1 g ∘ π1 f).
    eapply trCod. 1: eapply osym, mapComp, B. 
    exact (map (map B (π1 f)) (π2 g) ∘ π2 f).
  Defined.

  Definition totalId (p : totalCarrier) : totalHom p p.
  Proof.
    exists id.
    eapply trCod. 1: eapply osym, mapId, B.
    apply id.
  Defined.

  Definition totalCatStr : catStr totalCarrier :=
    {| hom := totalHom ; comp := totalComp; id := totalId |}.

  Notation "'Cast[' A '=>' B ']'" := (cast A B _) (only printing). 

  Lemma totalCatProp : catProp totalCatStr.
  Proof.
    constructor; intros; unshelve eapply pair_cong; cbn -[trCod].
    - eapply assoc, C.
    - eapply lunit, C.
    - eapply runit, C.
    - oleft (apply cast_trans).
      otrans. 
      2:{ eapply ap, osym. oleft (eapply ap, mapTrCod). 
          set (Bf := map B (π1 f));
          eapply (trCodPreComp (F:= map B (π1 h) ∘ map B (π1 g) ∘ Bf) (G:= map B (π1 h ∘ π1 g) ∘ Bf)).
      }
      eapply htranspose, otrans.
      1:{
        oleft (apply (trCodPostComp (G:=map B (π1 g ∘ π1 f)) (π2 h))).
        eapply ap, assoc, (B (π1 x)).
      }
      eapply htranspose, osym.
      do 3 oleft (eapply cast_trans).
      eapply htranspose.
      oleft (eapply (ap (-∘ π2 f)), (mapComp (map B (π1 f)))).
      reflexivity.
    - oleft (eapply cast_trans).
      eapply htranspose, otrans.
      1:{ 
        oleft (eapply ap, mapTrCod). 
        oleft (apply (trCodPreComp (F := map B (π1 f)) (G:= map B id ∘ map B (π1 f)))).
        eapply ap.
        oleft (eapply ap, mapId, (map B (π1 f))).
        apply lunit, (B (π1 x)).
      }
      eapply htranspose, osym; oleft (eapply cast_trans). 
      reflexivity.
    - oleft (eapply cast_trans).
      eapply htranspose.
      oleft (eapply (trCodPostComp (F := id Cat (B _)) (G:=map B id) (π2 f) _ id)).
      eapply htranspose.
      oleft (eapply runit, (B (π1 x))).
      osym; oleft (eapply cast_trans).
      reflexivity. 
  Qed.

  Definition totalCat : cat := 
    {| carrier := totalCarrier ; categoryStr := totalCatStr ; categoryEqs := totalCatProp |}.
  

  Definition projFunctorStr : functorStr totalCat C :=
    {| onObject (x : totalCat) := π1 x : C ; 
      map _ _ f := f.(π1)  
    |}.

  Definition projFunctorProp : functorProp projFunctorStr.
  Proof.  constructor; cbn; intros; reflexivity. Qed.

  Definition projFunctor : functor totalCat C :=
    {| functorStructure := projFunctorStr ; functorEqs := projFunctorProp |}.

End Grothendieck.

End GrothendieckFibration.




  

