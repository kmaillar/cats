From Cats Require Import Observational.

Set Universe Polymorphism.
Set Primitive Projections.
Set Observational Inductives.

(** SPROP *)

Inductive sBot : SProp :=.
Notation "⊥" := sBot.
Inductive sUnit : SProp := sunit : sUnit.
Notation "⊤" := sUnit.

Inductive Box (P : SProp) : Type := box : P -> Box P.
Arguments box {_} _.
Definition unbox {P} (x : Box P) : P := let '(box p) := x in p.


Inductive sor (P Q : SProp) : SProp :=
  | sinl : P -> sor P Q
  | sinr : Q -> sor P Q.
Arguments sinl {_ _} _.
Arguments sinr {_ _} _.
 
Polymorphic Inductive existS {X : Type} {P : X -> SProp} : SProp := exS (x : X) : P x -> existS.
Arguments existS : clear implicits.
Notation "∃s x , P" := (existS _ (fun x => P)) (x binder, P at next level, at level 50).

 
Set Primitive Projections.
(* Record ssigT{A : Type} {P : A -> SProp} : SProp := { stfst : A; stsnd : P stfst }. *)
Record sub {A : Type} {P : A -> SProp} : Type := { wit : A;  pf : P wit }.
Record ssig {A : SProp} {P : A -> SProp} : SProp := { sfst : A; ssnd : P sfst }.
Notation "A ∨ B" := (sor A B) (at level 55, right associativity).
Notation "A ∧ B" := (@ssig A (fun _ => B)) (at level 55, right associativity).
Notation "'Σ' x .. y ',' B" := (@ssig _ (fun x => .. (@ssig _ (fun y => B)) .. )) (x binder, y binder, B at next level, at level 60).
(* Notation "'ΣT' x .. y ',' B" := (@sub _ (fun x => .. (@sub _ (fun y => B)) .. )) (x binder, y binder, B at next level, at level 60). *)
Notation "'ΣT' x ',' B" := (@sub _ (fun x => B)) (x binder, B at next level, at level 60).
Notation "x ,;, y" := {| sfst := x; ssnd := y |} (at level 30, right associativity).

Notation "'¬' P" := (P -> ⊥) (at level 95).

Notation "P '<-->' Q" := ((P -> Q) ∧ (Q -> P)) (at level 95).

Lemma wit_cong {A : Type} {P : A -> SProp} {x y : ΣT a : A, P a} :
  wit x ~ wit y -> x ~ y.
Proof.
  intros eq; destruct x, y; cbn in *; induction eq; reflexivity.
Defined.



Class SReflexive {A : Type} (rel : A -> A -> SProp) : SProp :=
  { sreflexivity : forall (a : A), rel a a }.

Class SSymmetry {A : Type} (rel : A -> A -> SProp) : SProp :=
  { ssymmetry : forall (a b : A), rel a b -> rel b a }.

Class STransitivity {A : Type} (rel : A -> A -> SProp) : SProp :=
  { stransitivity : forall (a b c : A), rel a b -> rel b c -> rel a c }.

Lemma iff_sreflexivity : forall (A : SProp), A <--> A.
Proof. split; exact (fun x => x). Qed.

Lemma iff_ssymmetry : forall (A B : SProp), A <--> B -> B <--> A.
Proof. intros. destruct H. split; assumption. Qed.

Lemma iff_stransitivity : forall (A B C : SProp), A <--> B -> B <--> C -> A <--> C.
Proof.
  intros. destruct H, H0. split; auto. Qed.

Instance IffSReflexive : SReflexive (fun A B  => A <--> B) :=
  { sreflexivity := iff_sreflexivity }.

Instance IffSSymmetry : SSymmetry (fun A B  => A <--> B) :=
  { ssymmetry := iff_ssymmetry }.

Instance IffSTransitivity : STransitivity (fun A B  => A <--> B) :=
  { stransitivity := iff_stransitivity }.

Ltac sreflexivity := apply sreflexivity.
Ltac ssymmetry := apply ssymmetry.
Ltac stransitivity := apply stransitivity.
Ltac estransitivity := eapply stransitivity.

Ltac auto_sprop :=
  match goal with
  | [ |- sUnit ] => exact sunit
  | [ |- ?x ∧ ?y ] => split
  | [ |- Σ _ : ?a, ?b ] => split
  | [ |- ΣT _ : ?a, ?b ] => split
  end.

#[export]
Hint Extern 1 => auto_sprop.

#[export]
Hint Extern 0 => sreflexivity.


Section Join.
  (* In order to define the closed modality associated to a strict proposition *)
  Context {φ : SProp}.

  Definition join_rel (X : Type) : srel (X + Box φ) :=
    fun xo yo =>
      match xo, yo with
      | inr _ , _=> ⊤
      | _, inr _ => ⊤
      | inl x, inl y => x ~ y ∨ φ
      end.


  Definition join (X : Type) := 
    quot (X + Box φ) (join_rel X).

  Definition intoʲ {X} (x : X) : join X := into (inl x).
  Definition coll {X} (h : φ) : join X := into (inr (box h)). 
  Definition coll_eq {X} (x : X) (h : φ) : intoʲ x ~ coll h.
  Proof. apply quotEq; constructor. Qed.   

  Definition join_rect {X} (P : join X -> Type)
    (hinto : forall x, P (intoʲ x))
    (hcoll : forall h : φ, P (coll h))
    (hcoh : forall x (h : φ), hinto x ~[ap P (coll_eq x h)] hcoll h)
    (j : join X) : P j.
  Proof.
    unshelve eapply quot_rect.
    - intros [x|[h]]; [exact (hinto x)| exact (hcoll h)].
    - intros [x|[h]] [y|[h']]; cbn.
      + intros [eq | h].
        1: induction eq; reflexivity.
        eapply (htrans (hcoh x h)).
        apply (hsym0 (hcoh y h)).
      + intros; apply hcoh.
      + intros; apply (hsym (hcoh y h)).
      + reflexivity.
  Qed.


End Join.