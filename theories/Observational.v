(* -*- mode: coq; coq-prog-args: ("-allow-rewrite-rules") -*- *)

Set Universe Polymorphism.

(* The observational equality *)

Inductive obseq@{u} (A : Type@{u}) (a : A) : forall (b : A), SProp :=
| obseq_refl : obseq A a a.
Notation "a ~ b" := (obseq _ a b) (at level 50).
Arguments obseq {A} a _.
Arguments obseq_refl {A a} , [A] a.

Definition obseq_trans {A : Type} {a b c : A} (e : a ~ b) (e' : b ~ c) : a ~ c :=
  obseq_sind _ b (fun X _ => a ~ X) e c e'.
Notation "e @@@ f" := (obseq_trans e f) (at level 40, left associativity, only parsing).

Definition obseq_sym {A : Type} {a b : A} (e : a ~ b) : b ~ a :=
  obseq_sind _ a (fun X _ => X ~ a) obseq_refl b e.

(* The equality on the universe is annoying, because it needs a superfluous universe level.
   The other option is to have a second observational equality exclusively for universes: *)
Inductive obseqU@{u} (A : Type@{u}) : Type@{u} -> SProp :=
| obseqU_refl : obseqU A A.

(* Type casting *)
(* cast has two universe parameters because we use obseq instead of obseqU *)

Symbol cast@{u v} : forall (A B : Type@{u}), @obseq@{v} Type@{u} A B -> A -> B.
Notation "e # a" := (cast _ _ e a) (at level 55, only parsing).

(* SProp casting *)
(* We do not want to use sort polymorphism for cast, to avoid useless (and potentially looping)
   computations in SProp *)

Definition cast_prop (A B : SProp) (e : A ~ B) (a : A) := obseq_sind SProp A (fun X _ => X) a B e.
Notation "e #% a" := (cast_prop _ _ e a) (at level 40, only parsing).

Rewrite Rule cast_refl :=
| cast ?A ?A _ ?t ==> ?t.

Lemma cast_trans {A B C} (a : A) (AB : A ~ B) (BC : B ~ C) :
  (BC # (AB # a)) ~ (obseq_trans AB BC #  a).
Proof.
  revert a C BC.
  induction AB; reflexivity.
Qed.

(* We can use cast to derive large elimination principles for obseq *)

Definition ap {A B} (f : A -> B) {x y} (e : x ~ y) : f x ~ f y :=
  obseq_sind _ x (fun y _ => f x ~ f y) obseq_refl _ e.

Definition ap2 {A B C} (f : A -> B -> C) {x y x' y'} (e : x ~ y) (e' : x' ~ y') : 
  f x x' ~ f y y'.
Proof. induction e; induction e'; reflexivity. Qed.


Definition apd {A} {a} (P : forall b : A, a ~ b -> Type) (b : A) (e : a ~ b) : P a obseq_refl ~ P b e :=
  obseq_sind _ a (fun b e => P a obseq_refl ~ P b e) obseq_refl b e.

Definition obseq_rect@{u v v'} : forall (A : Type@{u}) (a : A) (P : forall b : A, obseq@{u} a b -> Type@{v}),
    P a obseq_refl@{u} -> forall (b : A) (e : obseq@{u} a b), P b e :=
  fun A a P t b e => cast@{v v'} (P a obseq_refl) (P b e) (apd P b e) t.

Definition obseq_rect'@{u v v'} : forall (A : Type@{u}) (a : A) (P : forall b : A, obseq@{u} a b -> Type@{v}),
    forall (b : A) (e : obseq@{u} a b), P b e -> P a obseq_refl@{u}.
Proof.
  induction e using obseq_rect@{u v v'}. trivial.
Defined.

Definition obseq_rec@{u v} : forall (A : Type@{u}) (a : A) (P : forall b : A, obseq@{u} a b -> Set),
    P a obseq_refl@{u} -> forall (b : A) (e : obseq@{u} a b), P b e :=
  fun A a P t b e => cast@{Set v} (P a obseq_refl) (P b e) (apd P b e) t.

(* The Prop eliminator is an axiom.
   The observational equality should not be used with Prop anyway. *)
Axiom obseq_ind@{u} : forall (A : Type@{u}) (a : A) (P : forall b : A, obseq@{u} a b -> Prop),
    P a obseq_refl@{u} -> forall (b : A) (e : obseq@{u} a b), P b e.

(** Definition of the observational equality on pi's *)

Parameter obseq_forall_1 : forall {A A' B B'}, (forall (x : A), B x) ~ (forall (x : A'), B' x) -> A' ~ A.
Parameter obseq_forall_2 : forall {A A' B B'} (e : (forall (x : A), B x) ~ (forall (x : A'), B' x)) (x : A'),
    B (obseq_forall_1 e # x) ~ B' x.

Parameter funext@{sa|a b u| a <= u, b <= u} : forall {A : Type@{sa|a}} {B : A -> Type@{b}} (f g : forall (x : A), B x), (forall (x : A), obseq@{b} (f x) (g x)) -> obseq@{u} f g.

Rewrite Rule cast_pi :=
| @{|u v+|+} |- cast@{u v} (forall (x : ?A), ?B) (forall (x : ?A'), ?B') ?e ?f
   ==> fun (x : ?A') => cast ?B@{x := cast ?A' ?A (obseq_forall_1@{v u u u u u v} ?e) x}
                             ?B'@{x := x}
                             (obseq_forall_2@{u u u u u v u v v} ?e x)
                             (?f (cast ?A' ?A (obseq_forall_1@{v u u u u u v} ?e) x)).

(** Definition of the observational equality on strict propositions *)

Parameter propext : forall {A B : SProp}, (A -> B) -> (B -> A) -> A ~ B.

(** Tests cast for functions *)

Section Basic_Test.
  Variable A B C : Set.
  Variable obseq_fun1 : (A -> C) ~ (B -> C).
  Variable obseq_fun2 : (C -> A) ~ (C -> B).
  Variable f : A -> C.
  Variable g : C -> A.

  (* remark that when the domain/codomain match, one of the casts is eliminated *)
  Eval lazy in (cast (A -> C) (B -> C) (obseq_fun1) f).
  Eval lazy in (cast (C -> A) (C -> B) (obseq_fun2) g).
  Eval lazy in (cast (A -> C) (A -> C) (obseq_refl _) f).

End Basic_Test.

(** Inductive types *)
Unset Universe Polymorphism.
Set Observational Inductives.

(* Declaring an inductive automaticall adds equalities and rewrite rules for cast *)
Inductive list (A : Type) : Type :=
| nil : list A
| cons : forall (a : A) (l : list A), list A.

(* Casting a singleton list *)
Section List_Test.

  Variable A B C : Set.
  Variable obseq_list : list A ~ list B.
  Variable a : A.

  Eval lazy in obseq_list # cons A a (nil A).
  Eval lazy in obseq_refl # cons A a (nil A).

End List_Test.

(* forded vectors *)

Inductive vect (A : Type) (n : nat) : Type :=
| vnil : n ~ 0 -> vect A n
| vcons : forall (a : A) (m : nat) (v : vect A m), n ~ S m -> vect A n.

Arguments vnil {A n e}.
Arguments vcons {A n} a m v {e}.

About obseq_vnil_0.
About obseq_vcons_0.
About obseq_vcons_1.
About obseq_vcons_2.

Notation vnil' := (vnil (e:= obseq_refl)).
Notation vcons' a n v := (vcons a n v (e := obseq_refl)).

(* equalities for vectors *)
Check (obseq_vnil_0:forall (A B : Type) (n m : nat), vect A n ~ vect B m -> (n ~ 0) ~ (m ~ 0)).
Print obseq_vcons_0.
Print obseq_vcons_1.
Print obseq_vcons_2.
Print obseq_vcons_3.

Section Vector_Test.

  Variable A B C : Set.
  Variable obseq_vect : forall {n m}, n ~ m -> vect A n ~ vect B m.
  Variable a : A.
  Variable n : nat.

  Eval lazy in (obseq_vect obseq_refl # vcons' a 0 vnil').
  Eval lazy in (obseq_refl # vcons' a 0 vnil').

End Vector_Test.


(* forded Martin-Löf identity type *)
Inductive Id (A : Type) (a : A) (b : A) : Set :=
| idrefl : forall (e : a ~ b), Id A a b.

Arguments idrefl {A a b}.

Notation idrefl' := (idrefl obseq_refl).

Print obseq_idrefl_0.

Lemma functional_extensionality A B (f g : A -> B) :
  (forall x y, Id A x y -> Id B (f x) (g y)) -> Id _ f g.
Proof.
  intro Heq; econstructor.
  eapply funext. intro x. specialize (Heq x x idrefl').
  destruct Heq; eauto.
Qed.



(** Additional notations and concepts *)

Set Universe Polymorphism.

Notation "x '~[' e ']' y" := ((e # x) ~ y) (at level 50).

Lemma htrans0 {A B C a b c} {AB : A ~ B} {BC : B ~ C} :
  a ~[AB] b -> b ~[BC] c -> a ~[obseq_trans AB BC] c.
Proof.
  intros ab bc.
  eapply obseq_trans; try eassumption.
  eapply obseq_trans.
  2: eapply (ap _ ab).
  apply obseq_sym, cast_trans.
Qed.

Set Printing Universes.
Lemma htrans {A B C a b c} {AB : A ~ B} {BC : B ~ C} {AC : A ~ C} :
  a ~[AB] b -> b ~[BC] c -> a ~[AC] c.
Proof.  exact (@htrans0 A B C a b c AB BC).  Qed.

Lemma htranspose {A B a b} {BA : B ~ A} : b ~ (obseq_sym BA # a) -> b ~[BA] a.
Proof. 
  intros ab.
  eapply obseq_trans.
  2: apply (cast_trans a (obseq_sym BA) BA).
  now apply ap.
Qed.

Lemma htranspose' {A B a} {AB : A ~ B} : a ~[AB] (AB # a).
Proof. reflexivity. Qed.

Lemma hsym {A B a b} {BA : B ~ A} : a ~[obseq_sym BA] b -> b ~[BA] a.
Proof. intros ab; exact (htranspose (obseq_sym ab)). Qed.

Lemma hsym0 {A B a b} {AB : A ~ B} : a ~[AB] b -> b ~[obseq_sym AB] a.
Proof. apply (@hsym A B a b (obseq_sym AB)). Qed.

Lemma hsym_alt {A B a b} {AB : A ~ B} {BA : B ~ A} : a ~[AB] b -> b ~[BA] a.
Proof.  apply (@hsym0 A B a b AB). Qed.

Lemma aph {A : Type} {B: A -> Type} {a1 a2 : A}  (f : forall a, B a) (eq : a1 ~ a2) :
  f a1 ~[ap B eq] f a2.
Proof.  induction eq; reflexivity. Qed.

(* Lemma aph {A : Type} {B C : A -> Type} {a1 a2 : A} {eq : a1 ~ a2} {b1 b2} (f : forall a, B a -> C a):
  b1 ~[ap B eq] b2 -> f a1 b1 ~[ap C eq] f a2 b2.
Proof.  induction eq; cbn; apply ap. Qed. *)

Notation srel I := (I -> I -> SProp).
Notation spred I := (I -> SProp).

(** Squash types *)
(* Required for existential quantification in HProp *)
Axiom quot@{u} : forall (A : Type@{u}) (R : srel A), Type@{u}.
Symbol into : forall A R, A -> quot A R.
Arguments into {_ _} _.
Axiom quotEq : forall A R (x y : A), R x y -> into (R:=R) x ~ into y.
Arguments quotEq {_ _ _ _} _.
Symbol quot_rect :
  forall A R (P : quot A R -> Type)
    (hinto : forall a, P (into a))
    (hquotEq : forall x y (r : R x y), hinto x ~[ap P (quotEq r)] hinto y),
    forall sqa, P sqa.

Symbol quot_sind :
  forall A R (P : quot A R -> SProp)
    (hinto : forall a, P (into a)),
    forall sqa, P sqa.

Rewrite Rule rewrite_quot_elim_into  :=
  | quot_rect ?A ?R ?P ?hinto ?heq (into ?a) ==> ?hinto ?a
  | quot_sind ?A ?R ?P ?hinto (into ?a) ==> ?hinto ?a.

Inductive rst_clos {A} (R : A -> A -> SProp) (x : A) : A -> SProp :=
  | rst_ret y : R x y -> rst_clos R x y
  | rst_refl : rst_clos R x x
  | rst_sym y : rst_clos R y x -> rst_clos R x y
  | rst_trans y z : rst_clos R x y -> rst_clos R y z -> rst_clos R x z.

(* Lemma quot_eq_characterization {A R} (x y : A) : into (R:=R) x ~ into y -> rst_clos R x y. 
Proof.

Lemma quot_onto {A R} (qx : quot A R) : ∃s x : A, into x ~ qx. *)



Polymorphic Record invertible {A B : Type} {f : A -> B} :=
  {
    invmap :> B -> A ;
    invsect : forall a, invmap (f a) ~ a ;
    invretr : forall b, f (invmap b) ~ b
  }.
Arguments invertible : clear implicits.
Arguments invertible {_ _} _.


