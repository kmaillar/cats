Cats
=========


Experimenting with categories in observational-tt.

Builds on top of patched Coq version available here:

https://github.com/loic-p/coq/tree/combo_ttobs

Once that coq version is installed (`opam pin install .` from the top directory of the coq folder), this project can be compiled with 
```
$ make
```


# Organisation of the Repository

- theories/Observational.v: basic observational type theory (taken from Loic and Nicolas's dev and modified)
- theories/Sprop.v: construction on strict propositions (taken from Mara's dev and modified)
- theories/Common.v: common type theoretic construction (for now only negative sigma types)
- theories/Cat.v: Definition of categories and functors, construction of the category Cat of categories, Grothendieck construction
